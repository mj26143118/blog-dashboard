import React from 'react';
import './index.css';
import { Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './pages/Home';
import ManagePost from './pages/ManagePost';


function App() {
  return (
    <div className="App">
          <div className="App">
      <h1>Welcome to My Blog</h1>
      {/* other components */}
    </div>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/manage/:postId" element={<ManagePost />} />
        <Route path="/manage" element={<ManagePost />} />
      </Routes>
    </div>
  );
}

export default App;
