import React, { useState, useEffect } from 'react';
import BlogForm from '../components/BlogForm';
import { useParams, useNavigate } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { usePosts } from '../contexts/PostsContext';

function ManagePost() {
  const { postId } = useParams();
  const navigate = useNavigate();
  const { posts, dispatch } = usePosts();
  const [showModal, setShowModal] = useState(true);
  const [formData, setFormData] = useState({});

  // Finding the post in the global state
  useEffect(() => {
    const post = posts.find(p => p.id === parseInt(postId));
    setFormData(post);
  }, [postId, posts]);

  const handleSaveChanges = () => {
    if (formData) {
      dispatch({ type: 'UPDATE_POST', payload: formData });
      setShowModal(false); 
      navigate('/'); // Redirects the user to the home page after save
    }
  };

  const handleClose = () => {
    setShowModal(false);
    navigate('/'); 
  };

  return (
    <Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Post</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {formData ? <BlogForm post={formData} onChange={setFormData} /> : 'Loading...'}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>Close</Button>
        <Button variant="primary" onClick={handleSaveChanges}>Save Changes</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ManagePost;
