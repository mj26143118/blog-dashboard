// Home.js
import React from 'react';
import { usePosts } from '../contexts/PostsContext';
import BlogPost from '../components/BlogPost';

function Home() {
  const { posts, dispatch } = usePosts();

  const handleDelete = (postId) => {
    dispatch({ type: 'DELETE_POST', payload: postId });
  };

  const handleCreateNew = () => {
    const newPost = {
      id: posts.length + 1,
      title: 'New Post',
      content: 'New content'
    };
    dispatch({ type: 'ADD_POST', payload: newPost });
  };

  return (
    <div>
      {posts.map(post => (
        <BlogPost key={post.id} post={post} onDelete={handleDelete} />
      ))}
      <button onClick={handleCreateNew}>Create New Post</button>
    </div>
  );
}

export default Home;
