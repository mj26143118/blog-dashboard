// src/index.js
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { PostsProvider } from './contexts/PostsContext';

ReactDOM.render(
  <BrowserRouter>
    <PostsProvider>
      <App />
    </PostsProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
