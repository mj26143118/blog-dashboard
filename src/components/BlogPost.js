// BlogPost.js
import React from 'react';
import { Link } from 'react-router-dom';

function BlogPost({ post, onDelete }) {
    return (
        <div className="post">
            <h3>{post.title}</h3>
            <p>{post.content}</p>
            <Link to={`/manage/${post.id}`} className="edit-button">Edit</Link>
            <button className="delete-button" onClick={() => onDelete(post.id)}>Delete</button>
        </div>
    );
}

export default BlogPost;
