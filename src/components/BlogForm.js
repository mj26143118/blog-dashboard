import React, { useState, useEffect } from 'react';

function BlogForm({ post, onChange }) {
  const [title, setTitle] = useState(post ? post.title : '');
  const [content, setContent] = useState(post ? post.content : '');

  useEffect(() => {
    onChange({ ...post, title, content });
  }, [title, content, onChange, post]);

  return (
    <form>
      <div className="form-group">
        <label htmlFor="title" style={{ marginRight: '10px', width: '70px' }}>Title:</label>
        <input 
          id="title" 
          type="text" 
          value={title} 
          onChange={e => setTitle(e.target.value)} 
          style={{ flex: 1, padding: '8px', borderRadius: '4px', border: '1px solid #ccc' }}
        />
      </div>
      <div className="form-group">
        <label htmlFor="content" style={{ marginRight: '10px', width: '70px' }}>Content:</label>
        <textarea 
          id="content" 
          value={content} 
          onChange={e => setContent(e.target.value)}
          style={{ flex: 1, padding: '8px', borderRadius: '4px', border: '1px solid #ccc', minHeight: '100px' }}
        />
      </div>
    </form>
  );
}

export default BlogForm;
