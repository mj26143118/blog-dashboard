// src/contexts/PostsContext.js
import React, { createContext, useContext, useReducer } from 'react';

const PostsContext = createContext();

const initialState = [
    { id: 1, title: 'Post 1', content: 'Content 1' },
    { id: 2, title: 'Post 2', content: 'Content 2' }
];

function postsReducer(state, action) {
  switch (action.type) {
    case 'UPDATE_POST':
      return state.map(post => post.id === action.payload.id ? action.payload : post);
    case 'ADD_POST':
      return [...state, action.payload];
    case 'DELETE_POST':
      return state.filter(post => post.id !== action.payload);
    default:
      return state;
  }
}

export const PostsProvider = ({ children }) => {
  const [posts, dispatch] = useReducer(postsReducer, initialState);

  return (
    <PostsContext.Provider value={{ posts, dispatch }}>
      {children}
    </PostsContext.Provider>
  );
};

export const usePosts = () => useContext(PostsContext);
